#!/bin/sh
set -e

mkdir ~/.local/share/fonts

cd ~/.vim_runtime/fonts/SourceCodePro/Regular/complete/
mv Sauce\ Code\ Pro\ Nerd\ Font\ Complete\ Mono\ Windows\ Compatible.ttf Sauce\ Code\ Pro\ Nerd\ Font\ Complete\ Mono.ttf Sauce\ Code\ Pro\ Nerd\ Font\ Complete\ Windows\ Compatible.ttf Sauce\ Code\ Pro\ Nerd\ Font\ Complete.ttf ~/.local/share/fonts/

cd ~/.vim_runtime

echo 'set runtimepath+=~/.vim_runtime
source ~/.vim_runtime/vimrcs/basic.vim
source ~/.vim_runtime/vimrcs/filetypes.vim
source ~/.vim_runtime/vimrcs/plugins_config.vim
source ~/.vim_runtime/vimrcs/extended.vim
try
source ~/.vim_runtime/my_configs.vim
catch
endtry' > ~/.vimrc

vim -c 'PlugInstall'


echo "Selamat anda berhasil menginstall L.O.G VIMRC !"