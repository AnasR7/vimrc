![LOGO](https://gitlab.com/AnasR7/vimrc/raw/master/img/vimrcultimate.png)


for project website [LINK](https://log-vimrc.gitlab.io/)
# Attention !

Before you install this Mod, install Prettier and (NerdFonts <<::This is optional) for fully customize

* Intall prettier with NPM

        npm install prettier
        
* Install NerdFonts for windows install manually

        https://github.com/ryanoasis/nerd-fonts#option-3-install-script

### this Optional
* Download NerdFonts in AUR

        yaourt -S nerd-fonts-complete


# For Windows
* Install Python3
* Install gvim8.exe
* After install gvim and python open gvim and type `:echo has("python3")` if value is () is meaning failed, but if 1 the installation is work.
* Install GIT(console) and after that doit vimrc configuration.

## vimrc Configuration
 * Copy this Line to your terminal.
 
        git clone --depth=1 https://gitlab.com/AnasR7/vimrc.git ~/.vim_runtime
        sh ~/.vim_runtime/install_myvimrc.sh
        

* Open Vim or GVim
* Type :PlugInstall
* Wait until done..!
* close with type :q twice
* Open Vim or Gvim again
* Happy Code...
