"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"=>> plugins_config.vim
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""
" => Load VIM PLUG 
""""""""""""""""""""""""""""""
" YCM on vim plug
function! BuildYCM(info)
  " info is a dictionary with 3 fields
  " - name:   name of the plugin
  " - status: 'installed', 'updated', or 'unchanged'
  " - force:  set on PlugInstall! or PlugUpdate!
  if a:info.status == 'installed' || a:info.force
    !./install.py
  endif
endfunction
" Specify a directory for plugins
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim_runtime/plugged')
" Make sure you use single quotes
Plug 'https://github.com/mileszs/ack.vim.git'
Plug 'https://github.com/vim-syntastic/syntastic.git'
Plug 'https://github.com/jiangmiao/auto-pairs.git'
Plug 'https://github.com/yuttie/comfortable-motion.vim.git'
Plug 'https://github.com/junegunn/goyo.vim.git'
Plug 'https://github.com/yegappan/mru.git'
Plug 'https://github.com/scrooloose/nerdtree.git'
Plug 'https://github.com/godlygeek/tabular.git'
Plug 'https://github.com/kchmck/vim-coffee-script.git'
Plug 'https://github.com/tpope/vim-commentary.git'
Plug 'https://github.com/terryma/vim-expand-region.git'
Plug 'https://github.com/nvie/vim-flake8.git'
Plug 'https://github.com/tpope/vim-fugitive.git'
Plug 'https://github.com/airblade/vim-gitgutter.git'
Plug 'https://github.com/michaeljsmith/vim-indent-object.git'
Plug 'https://github.com/groenewege/vim-less.git'
Plug 'https://github.com/plasticboy/vim-markdown.git'
Plug 'https://github.com/terryma/vim-multiple-cursors.git'
Plug 'https://github.com/tpope/vim-repeat.git'
Plug 'https://github.com/tpope/vim-surround.git'
Plug 'https://github.com/maxbrunsfeld/vim-yankstack.git'
Plug 'https://github.com/amix/vim-zenroom2.git'
Plug 'https://github.com/nightsense/office.git'
Plug 'https://gitlab.com/AnasR7/vim-airline.git'
Plug 'https://gitlab.com/AnasR7/vim-airline-themes.git'
Plug 'https://github.com/altercation/vim-colors-solarized.git'
Plug 'https://github.com/ajmwagar/vim-deus.git'
Plug 'https://github.com/junegunn/fzf.git', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'https://gitlab.com/AnasR7/emmet-vim.git'
Plug 'https://github.com/andymass/vim-matchup.git'
Plug 'https://github.com/mikewest/vimroom.git'
Plug 'https://github.com/posva/vim-vue.git'
Plug 'https://github.com/Glench/Vim-Jinja2-Syntax.git'
Plug 'https://github.com/ap/vim-css-color.git'
Plug 'https://github.com/leafgarland/typescript-vim.git'
Plug 'https://github.com/pangloss/vim-javascript.git'
Plug 'https://github.com/w0rp/ale.git'
Plug 'https://github.com/mjbrownie/vim-htmldjango_omnicomplete.git'
Plug 'https://github.com/davidhalter/jedi-vim.git'
Plug 'https://github.com/tweekmonster/django-plus.vim.git'
Plug 'https://github.com/jmcomets/vim-pony.git'
Plug 'https://github.com/isRuslan/vim-es6.git'
Plug 'https://gitlab.com/AnasR7/vim-startify.git'
Plug 'https://github.com/ryanoasis/vim-devicons.git'
Plug 'https://github.com/ryanoasis/vim-webdevicons.git'
Plug 'https://github.com/tiagofumo/vim-nerdtree-syntax-highlight.git'
Plug 'https://github.com/bagrat/vim-workspace.git'
Plug 'https://github.com/Xuyuanp/nerdtree-git-plugin.git'
Plug 'https://github.com/vwxyutarooo/nerdtree-devicons-syntax.git'
Plug 'https://gitlab.com/AnasR7/vim-janah.git'
Plug 'https://github.com/sjl/gundo.vim.git'
Plug 'https://github.com/luochen1990/rainbow.git'
Plug 'https://github.com/othree/html5.vim.git'
Plug 'https://github.com/ayu-theme/ayu-vim.git'
Plug 'https://github.com/rhysd/vim-color-spring-night.git'
Plug 'https://github.com/ivanov/vim-ipython.git'
Plug 'https://github.com/vim-python/python-syntax.git'
Plug 'https://github.com/sentientmachine/pretty-vim-python.git'
Plug 'https://github.com/sheerun/vim-polyglot.git'
Plug 'https://github.com/jelera/vim-javascript-syntax.git'
Plug 'https://github.com/eslint/eslint.git'
" Initialize plugin system
call plug#end()


""""""""""""""""""""""""""""""
" => vim-ALE 
""""""""""""""""""""""""""""""
let g:ale_completion_enabled = 1


""""""""""""""""""""""""""""""
" => vim-JEDI 
""""""""""""""""""""""""""""""
let g:jedi#use_splits_not_buffers = "left"
autocmd FileType python setlocal completeopt-=preview
let g:jedi#show_call_signatures = "1"
let g:jedi#completions_enabled = 1


""""""""""""""""""""""""""""""
" => vim-Gundo 
""""""""""""""""""""""""""""""
nnoremap <F2> :GundoToggle<CR>


""""""""""""""""""""""""""""""
" => vim-Javascript 
""""""""""""""""""""""""""""""
let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_ngdoc = 1
let g:javascript_plugin_flow = 1
augroup javascript_folding
    au!
    au FileType javascript setlocal foldmethod=syntax
augroup END


""""""""""""""""""""""""""""""
" => vim-MatchUp  
""""""""""""""""""""""""""""""
let g:loaded_matchit = 1


""""""""""""""""""""""""""""""
" => vim-htmldjango_omnicomplete  
""""""""""""""""""""""""""""""
au BufNewFile,BufRead *.html set filetype=htmldjango

au FileType htmldjango set omnifunc=htmldjangocomplete#CompleteDjango
let g:htmldjangocomplete_html_flavour = 'html5'
au FileType htmldjango inoremap {% {% %}<left><left><left>
au FileType htmldjango inoremap {{ {{ }}<left><left><left>



""""""""""""""""""""""""""""""
" => Rainbow Parentheses Improved  
""""""""""""""""""""""""""""""
let g:rainbow_active = 1 "0 if you want to enable it later via :RainbowToggle


""""""""""""""""""""""""""""""
" =>DevICONS  
""""""""""""""""""""""""""""""
 function! StartifyEntryFormat()
   return 'WebDevIconsGetFileTypeSymbol(absolute_path) ." ". entry_path'
 endfunction
 let g:webdevicons_enable = 1
 let g:webdevicons_enable_nerdtree = 1
 let g:DevIconsEnableFoldersOpenClose = 1
 let g:webdevicons_enable = 1
 let g:webdevicons_enable_nerdtree = 1
 let g:webdevicons_enable_airline_tabline = 1
 let g:webdevicons_enable_airline_statusline = 1
 let g:webdevicons_conceal_nerdtree_brackets = 1
""""""""""""""""""""""""""""""
" => Syntastic 
""""""""""""""""""""""""""""""
"Recommend for newbie user
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0


""""""""""""""""""""""""""""""
" => EMMET VIM
""""""""""""""""""""""""""""""
let g:user_emmet_leader_key = ','


""""""""""""""""""""""""""""""
" => PyMODE (Python Mode)
""""""""""""""""""""""""""""""
let g:pymode_python = 'python3'
let g:pymode_options_max_line_length=120

""""""""""""""""""""""""""""""
" => MRU plugin
""""""""""""""""""""""""""""""
let MRU_Max_Entries = 400
map <leader>f :MRU<CR>


""""""""""""""""""""""""""""""
" => YankStack
""""""""""""""""""""""""""""""
let g:yankstack_yank_keys = ['y', 'd']

nmap <c-p> <Plug>yankstack_substitute_older_paste
nmap <c-n> <Plug>yankstack_substitute_newer_paste


""""""""""""""""""""""""""""""
" => ZenCoding
""""""""""""""""""""""""""""""
" Enable all functions in all modes
let g:user_zen_mode='a'


""""""""""""""""""""""""""""""
" => Nerd Tree
""""""""""""""""""""""""""""""
let g:NERDTreeWinPos = "left"
let NERDTreeShowHidden=0
let NERDTreeIgnore = ['\.pyc$', '__pycache__']
let g:NERDTreeWinSize=35
map <leader>nn :NERDTreeToggle<cr>
map <leader>nb :NERDTreeFromBookmark<Space>
map <leader>nf :NERDTreeFind<cr>


""""""""""""""""""""""""""""""
" => Nerd Tree Highlight Syntax
""""""""""""""""""""""""""""""
let g:NERDTreeHighlightFolders = 1 " enables folder icon highlighting using exact match
let g:NERDTreeHighlightFoldersFullName = 1 " highlights the folder name
let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1

""""""""""""""""""""""""""""""
" => vim-multiple-cursors
""""""""""""""""""""""""""""""
let g:multi_cursor_use_default_mapping=0

" Default mapping
let g:multi_cursor_start_word_key      = '<C-s>'
let g:multi_cursor_select_all_word_key = '<A-s>'
let g:multi_cursor_start_key           = 'g<C-s>'
let g:multi_cursor_select_all_key      = 'g<A-s>'
let g:multi_cursor_next_key            = '<C-s>'
let g:multi_cursor_prev_key            = '<C-p>'
let g:multi_cursor_skip_key            = '<C-x>'
let g:multi_cursor_quit_key            = '<Esc>'


""""""""""""""""""""""""""""""
" => surround.vim config
""""""""""""""""""""""""""""""
vmap Si S(i_<esc>f)
au FileType mako vmap Si S"i${ _(<esc>2f"a) }<esc>


""""""""""""""""""""""""""""""
" => Airline and Airline-Themes
""""""""""""""""""""""""""""""
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_theme='ayu_mirage'
let g:airline_powerline_fonts = 1


""""""""""""""""""""""""""""""
" => Vimroom
""""""""""""""""""""""""""""""
let g:goyo_width=100
let g:goyo_margin_top = 2
let g:goyo_margin_bottom = 2
nnoremap <silent> <leader>z :Goyo<cr>


""""""""""""""""""""""""""""""
" => Syntastic (syntax checker)
""""""""""""""""""""""""""""""
let g:ale_linters = {
\   'javascript': ['jshint'],
\   'python': ['flake8'],
\   'go': ['go', 'golint', 'errcheck']
\}

nmap <silent> <leader>a <Plug>(ale_next_wrap)

" Disabling highlighting
let g:ale_set_highlights = 0

" Only run linting when saving the file
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_enter = 0


"""""""""""""""""""""""""""""""
" => Git gutter (Git diff)
"""""""""""""""""""""""""""""""
let g:gitgutter_enabled=0
nnoremap <silent> <leader>d :GitGutterToggle<cr>
